package pl.vpplant.sensors.monitoring.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class DataSeriesLineChartController implements Initializable {
    private static final Logger LOGGER = Logger.getLogger(DataSeriesLineChartController.class.getName());

    @FXML
    private Pane dataSeriesLineChartPaneId;

    private LineChart<String, Number> lineChart;

    public DataSeriesLineChartController() {}

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();

        xAxis.setLabel("Date and Time");
        yAxis.setLabel("Sensor Value");

        lineChart = new LineChart<>(xAxis, yAxis);
        lineChart.setPrefSize(900, 600);
        lineChart.setTitle("Sensors Monitoring");

        this.dataSeriesLineChartPaneId.getChildren().add(this.lineChart);
    }

    public LineChart<String, Number> getLineChart() {
        return lineChart;
    }
}
