package pl.vpplant.sensors.monitoring.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.vpplant.sensors.monitoring.dao.DataSeriesDao;
import pl.vpplant.sensors.monitoring.dao.model.DataSeriesModel;
import pl.vpplant.sensors.monitoring.service.DataSeriesGroupService;
import pl.vpplant.sensors.monitoring.service.DataSeriesLineChartService;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class SensorPickerController implements Initializable {

    private static final Logger LOGGER = Logger.getLogger(SensorPickerController.class.getName());

    @FXML
    private ComboBox<String> dataSeriesGroupComboBoxId;
    @FXML
    private ComboBox<DataSeriesModel> dataSeriesComboBoxId;
    @FXML
    private Button pickSensorButtonId;

    private Stage primaryStage;
    private Stage chartStage;
    private DataSeriesLineChartController dataSeriesLineChartController;

    private DataSeriesDao dataSeriesDao;
    private DataSeriesGroupService dataSeriesGroupService;

    private List<DataSeriesModel> dataSeriesModelList;
    private List<String> dataSeriesGroupModelList;

    public SensorPickerController() {}

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.dataSeriesDao = new DataSeriesDao();
        this.dataSeriesGroupService = new DataSeriesGroupService(this.dataSeriesDao);

        this.dataSeriesModelList = this.dataSeriesDao.selectData();
        this.dataSeriesGroupModelList = this.dataSeriesGroupService.selectData();

        showChart();

        ObservableList<String> dataSeriesGroupsObservableList =
                FXCollections.observableArrayList(dataSeriesGroupModelList);
        this.dataSeriesGroupComboBoxId.setItems(dataSeriesGroupsObservableList);

        // Data Series Group ComboBox Action
        this.dataSeriesGroupComboBoxId.setOnAction(event -> {
            String selectedDataSeriesGroup = this.dataSeriesGroupComboBoxId.getValue();
            ObservableList<DataSeriesModel> dataSeriesModelObservableList =
                    FXCollections.observableArrayList(filterBySensorGroup(selectedDataSeriesGroup));
            this.dataSeriesComboBoxId.setItems(dataSeriesModelObservableList);
        });

        // Pick Sensor Button Action
        this.pickSensorButtonId.setOnAction(event -> {
            LOGGER.info("sensor picked");
            DataSeriesModel dataSeriesModel = this.dataSeriesComboBoxId.getValue();
            LOGGER.info("" + dataSeriesModel);

            DataSeriesLineChartService.addSeries(
                    dataSeriesModel.getCharId(),
                    this.dataSeriesLineChartController.getLineChart(),
                    filterBySensor(dataSeriesModel));

            this.chartStage.show();
        });
    }

    private void showChart() {
        try {
//            FXMLLoader loader = new FXMLLoader(getClass().getResource("views/dataSeriesLineChart.fxml"));
            FXMLLoader loader = new FXMLLoader(SensorPickerController.class.getClassLoader().getResource("views/dataSeriesLineChart.fxml"));
            Pane pane = loader.load();

            this.dataSeriesLineChartController = loader.getController();

            this.chartStage = new Stage();
            this.chartStage.setTitle("Monitoring Chart");
            this.chartStage.initModality(Modality.APPLICATION_MODAL);
            this.chartStage.initOwner(primaryStage);

            Scene scene = new Scene(pane);
            this.chartStage.setScene(scene);
            this.chartStage.show();
        } catch (IOException e) {
            LOGGER.severe("Problem while loading dataSeriesLineChart.fxml");
        }
    }

    private List<DataSeriesModel> filterBySensorGroup(String selected) {
        LOGGER.info("filterBySensorGroup(" + selected + ")");

        dataSeriesModelList = dataSeriesDao.selectData();

        Map<String, DataSeriesModel> dataSeriesModelMap = new HashMap<>();

        for (DataSeriesModel model : this.dataSeriesModelList) {
            String charId = model.getCharId();
            if (charId.contains(selected)) {
                dataSeriesModelMap.put(charId, model);
            }
        }

        return new ArrayList<>(dataSeriesModelMap.values());
    }

    private List<DataSeriesModel> filterBySensor(DataSeriesModel dataSeriesModel) {
        LOGGER.info("filterBySensor(" + dataSeriesModel + ")");

        List<DataSeriesModel> dataSeriesModelFilteredList = new ArrayList<>();

        for (DataSeriesModel model : this.dataSeriesModelList) {
            String modelCharId = model.getCharId();
            if (modelCharId.equalsIgnoreCase(dataSeriesModel.getCharId())) {
                dataSeriesModelFilteredList.add(model);
            }
        }

        return dataSeriesModelFilteredList;
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }
}
