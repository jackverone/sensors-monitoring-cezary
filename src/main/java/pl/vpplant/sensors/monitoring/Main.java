package pl.vpplant.sensors.monitoring;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pl.vpplant.sensors.monitoring.controller.SensorPickerController;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
//        FXMLLoader loader = new FXMLLoader(getClass().getResource("controller/sensorPicker.fxml"));
        FXMLLoader loader = new FXMLLoader(Main.class.getClassLoader().getResource("views/sensorPicker.fxml"));
        Parent root = loader.load();

        SensorPickerController controller = loader.getController();
        controller.setPrimaryStage(primaryStage);

        primaryStage.setTitle("Monitoring");
        primaryStage.setScene(new Scene(root, 900, 300));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
