package pl.vpplant.sensors.monitoring.service;

import pl.vpplant.sensors.monitoring.dao.DataSeriesDao;
import pl.vpplant.sensors.monitoring.dao.model.DataSeriesModel;

import java.util.List;
import java.util.stream.Collectors;

public class DataSeriesGroupService {
    private DataSeriesDao dataSeriesDao;

    public DataSeriesGroupService(DataSeriesDao dataSeriesDao) {
        this.dataSeriesDao = dataSeriesDao;
    }

    public List<String> selectData() {
        List<DataSeriesModel> dataSeriesModels = this.dataSeriesDao.selectData();

        List<String> strings = dataSeriesModels.stream()
                .map(model -> {
                    String[] split = model.getCharId().split("\\.");
                    return split[split.length - 2];
                })
                .distinct()
                .sorted()
                .collect(Collectors.toList());

        return strings;
    }
}
