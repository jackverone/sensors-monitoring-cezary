package pl.vpplant.sensors.monitoring.service;

import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import pl.vpplant.sensors.monitoring.dao.model.DataSeriesModel;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.logging.Logger;

public class DataSeriesLineChartService {

    private static final Logger LOGGER = Logger.getLogger(DataSeriesLineChartService.class.getName());
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("MM-dd");
//        DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");

    public static void addSeries(String name, LineChart<String, Number> lineChart, List<DataSeriesModel> models) {
        XYChart.Series series = new XYChart.Series();
        series.setName(name);

        for (DataSeriesModel model : models) {
            String formattedLastChange = DATE_TIME_FORMATTER.format(model.getLastChange());
//            LOGGER.info(model.getLastChange() + "");
//            LOGGER.info(formattedLastChange);

            series.getData().add(new XYChart.Data(formattedLastChange, model.getLastValue()));
        }

        lineChart.getData().add(series);
    }
}
